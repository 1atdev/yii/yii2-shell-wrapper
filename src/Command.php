<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper;

use Exception;
use IsAtDev\ShellWrapper\Commands\AbstractCommand;
use IsAtDev\ShellWrapper\Commands\Argument;
use IsAtDev\ShellWrapper\Commands\Collections\Arguments;
use IsAtDev\ShellWrapper\Commands\Collections\Flags;
use IsAtDev\ShellWrapper\Commands\Collections\Params;
use IsAtDev\ShellWrapper\Commands\Collections\SubCommands;
use IsAtDev\ShellWrapper\Commands\Flag;
use IsAtDev\ShellWrapper\Commands\Param;
use IsAtDev\ShellWrapper\Commands\SubCommand;
use IsAtDev\ShellWrapper\Runners\DryRunner;
use IsAtDev\ShellWrapper\Runners\Exec;
use IsAtDev\ShellWrapper\Runners\FakeRunner;
use IsAtDev\ShellWrapper\Runners\Passthru;
use IsAtDev\ShellWrapper\Runners\ShellExec;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class Command
 *
 * @property-write string $command
 */
final class Command extends AbstractCommand
{
	private Arguments $_arguments;
	private Flags $_flags;
	private Params $_params;
	private SubCommands $_subCommands;
	
	private $_separateArgValue = true;
	
	private $_debugMode = false;
	
	public static function begin(string $command)
	{
		return new self(['command' => $command]);
	}
	
	public function init()
	{
		$this->_arguments = new Arguments();
		$this->_flags = new Flags();
		$this->_params = new Params();
		$this->_subCommands = new SubCommands();
	}
	
	public function __toString()
	{
		return sprintf(
			'%s%s%s%s%s',
			$this->_command,
			$this->pad($this->_subCommands),
			$this->pad($this->_flags),
			$this->pad($this->_arguments),
			$this->pad($this->_params)
		);
	}
	
	public function __clone()
	{
		$this->_arguments = clone $this->_arguments;
		$this->_flags = clone $this->_flags;
		$this->_params = clone $this->_params;
		$this->_subCommands = clone $this->_subCommands;
	}
	
	public function debugMode()
	{
		$this->_debugMode = true;
		
		return $this;
	}
	
	public function addSubCommand(SubCommand $command): void
	{
		$this->_subCommands->addSubCommand($command);
	}
	
	public function addParam(Param $param): void
	{
		$this->_params->addParam($param);
	}
	
	public function addArgument(Argument $argument): void
	{
		if ($this->_separateArgValue) {
			$argument->separateArgValues();
		}
		
		$this->_arguments->addArgument($argument);
	}
	
	public function separateArgValues()
	{
		$this->_separateArgValue = true;
		
		return $this;
	}
	
	public function addFlag(Flag $flag): void
	{
		if ($this->_separateArgValue) {
			$flag->separateArgValues();
		}
		
		$this->_flags->addFlag($flag);
	}
	
	/**
	 * @param array $configs
	 *
	 * @return Exec|ShellExec
	 * @throws Exception
	 */
	public function exec(array $configs = [])
	{
		$mode = ArrayHelper::getValue($configs, 'mode', 'exec');
		
		if ($this->_debugMode) {
			if (php_sapi_name() == 'cli') {
				Console::stdout("\n\nCmdToRun: $this\n\n");
			} else {
				Yii::debug("CmdToRun: $this\n");
			}
		}
		
		switch ($mode) {
			case 'shell':
				$exec = new ShellExec();
				break;
			case 'pass':
				$exec = new Passthru();
				break;
			case 'fake':
				$exec = new FakeRunner();
				break;
			case 'dry':
				$exec = new DryRunner();
				break;
			default:
				$exec = new Exec();
		}
		
		$exec->run($this);
		
		return $exec;
	}
	
	private function pad($string): string
	{
		$string = (string)$string;
		
		if (!empty($string)) {
			return " $string";
		}
		
		return $string;
	}
	
	
}
