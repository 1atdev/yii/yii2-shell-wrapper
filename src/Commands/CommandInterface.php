<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands;

interface CommandInterface
{
	public function __toString();
}
