<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands;

class SubCommand extends AbstractCommand
{
	public function __toString()
	{
		return $this->command;
	}
}
