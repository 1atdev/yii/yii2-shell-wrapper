<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands;

use Exception;
use IsAtDev\ShellWrapper\Command;
use IsAtDev\ShellWrapper\Runners\Exec;
use IsAtDev\ShellWrapper\Runners\ShellExec;
use yii\base\BaseObject;

/**
 * Class Builder
 *
 * @property-write string $command
 */
class Builder extends BaseObject implements CommandInterface
{
	/**
	 * @var string
	 */
	private $_command = '';
	
	/**
	 * @var Command
	 */
	private $_baseCommand;
	
	/**
	 * @var bool
	 */
	private $_debugMode = false;
	
	public function init()
	{
		$this->_baseCommand = Command::begin($this->_command);
	}
	
	public function __toString()
	{
		return (string)$this->_baseCommand;
	}
	
	public function setCommand(string $command): Builder
	{
		$this->_command = $command;
		
		$this->_baseCommand->setCommand($command);
		
		return $this;
	}
	
	public function separateArgValues()
	{
		$this->_baseCommand->separateArgValues();
		
		return $this;
	}
	
	public function addSubCommand(string $subCommand): static
	{
		$this->_baseCommand->addSubCommand(new SubCommand(['command' => $subCommand]));
		
		return $this;
	}
	
	public function addArgument(string $name, string $value = null): static
	{
		$this->_baseCommand->addArgument(new Argument($name, $value));
		
		return $this;
	}
	
	public function addFlag(string $flag, string $value = null): static
	{
		$this->_baseCommand->addFlag(new Flag($flag, $value));
		
		return $this;
	}
	
	public function addParam(string $param): static
	{
		$this->_baseCommand->addParam(new Param($param));
		
		return $this;
	}
	
	/**
	 * @param array $configs
	 *
	 * @return Exec|ShellExec
	 * @throws Exception
	 */
	public function exec(array $configs = [])
	{
		if ($this->_debugMode) {
			return $this->_baseCommand->debugMode()->exec($configs);
		}
		
		return $this->_baseCommand->exec($configs);
	}
	
	public function debugMode()
	{
		$this->_debugMode = true;
		
		return $this;
	}
}
