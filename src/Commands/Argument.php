<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands;

class Argument extends Value
{
	public const string PREFIX = '--';
}
