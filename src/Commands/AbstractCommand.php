<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands;

use yii\base\BaseObject;

/**
 * @property-write  string $command
 */
abstract class AbstractCommand extends BaseObject implements CommandInterface
{
	/**
	 * @var string
	 */
	protected $_command;
	
	public function setCommand(string $command): self
	{
		$this->_command = $command;
		
		return $this;
	}
	
	abstract public function __toString();
}
