<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands;

class Flag extends Value
{
	public const string PREFIX = '-';
}
