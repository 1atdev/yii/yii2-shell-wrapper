<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands;

abstract class Value
{
	protected string $name;
	protected array|null $values;
	protected bool $separateArgValue = false;
	
	public function __construct(string $name, array|string|null $values = null)
	{
		$this->name = $name;
		
		if (!is_array($values) && $values !== null) {
			$values = [$values];
		}
		
		$this->values = $values;
	}
	
	public function __toString()
	{
		if ($this->values === null) {
			return sprintf('%s%s', static::PREFIX, $this->name);
		}
		
		return $this->getValuesAsString();
	}
	
	protected function getValuesAsString(): string
	{
		$values = array_map('escapeshellarg', $this->values);
		$prefix = sprintf('%s%s ', static::PREFIX, $this->name);
		
		if ($this->separateArgValue) {
			$prefix = trim($prefix);
			
			return $prefix . "=" . join(" $prefix=", $values);
		}
		
		return $prefix . join(" $prefix", $values);
	}
	
	public function separateArgValues()
	{
		$this->separateArgValue = true;
		
		return $this;
	}
	
	public function getName(): string
	{
		return $this->name;
	}
}
