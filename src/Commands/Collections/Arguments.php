<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Commands\Collections;

use IsAtDev\ShellWrapper\Commands\Argument;

class Arguments
{
	protected array $arguments = [];
	
	public function __toString()
	{
		return join(' ', $this->arguments);
	}
	
	public function addArgument(Argument $argument): void
	{
		$this->arguments[$argument->getName()] = $argument;
	}
}
