<?php

namespace IsAtDev\ShellWrapper\Commands;

/**
 * Class MySQL
 */
final class MySQL extends Builder
{
	public function init()
	{
		parent::init();
		
		$this->setCommand('mysql');
		
		$this->separateArgValues();
	}
	
	/**
	 * Connect to the MySQL server on the given host.
	 *
	 * @param string $host
	 *
	 * @return $this
	 */
	public function host(string $host)
	{
		$this->addArgument('host', $host);
		
		return $this;
	}
	
	/**
	 * The TCP/IP port number to use for the connection or 0 for default to, in order of preference, my.cnf, $MYSQL_TCP_PORT, /etc/services, built-in default (3306).
	 * Forces --protocol=tcp when specified on the command line without other connection properties.
	 *
	 * @param int|string $port
	 *
	 * @return $this
	 */
	public function port(int|string $port)
	{
		$this->addArgument('port', $port);
		
		return $this;
	}
	
	/**
	 * The connection protocol to use for connecting to the server. It is useful when the other connection parameters normally would cause a protocol to be used other than the one you want.
	 *
	 * @param string $protocol Accepted values is TCP|SOCKET|PIPE|MEMORY
	 *
	 * @return $this
	 */
	public function protocol(string $protocol)
	{
		$this->addArgument('protocol', $protocol);
		
		return $this;
	}
	
	/**
	 * The MySQL user name to use when connecting to the server
	 *
	 * @param string $user
	 *
	 * @return $this
	 */
	public function user(string $user)
	{
		$this->addArgument('user', $user);
		
		return $this;
	}
	
	/**
	 * The password to use when connecting to the server. If you use the short option form (-p), you cannot have a space between the option and the password.
	 * If you omit the password value following the --password or -p option on the command line, mysql prompts for one.
	 *
	 * @param string $password
	 *
	 * @return $this
	 */
	public function password(string $password)
	{
		$this->addArgument('password', $password);
		
		return $this;
	}
	
	/**
	 * The database to use.
	 *
	 * @param string $database
	 *
	 * @return $this
	 */
	public function database(string $database)
	{
		$this->addArgument('database', $database);
		
		return $this;
	}
	
	/**
	 * Silent mode. Produce less output. This option can be given multiple times to produce less and less output.
	 * This option results in nontabular output format and escaping of special characters.
	 * Escaping may be disabled by using raw mode; see the description for the --raw option.
	 *
	 * @return $this
	 */
	public function silent()
	{
		$this->addArgument('silent');
		
		return $this;
	}
	
	/**
	 * For connections to localhost, the Unix socket file to use, or, on Windows, the name of the named pipe to use.
	 * Forces --protocol=socket when specified on the command line without other connection properties; on Windows, forces --protocol=pipe.
	 *
	 * @param string $socket
	 *
	 * @return $this
	 */
	public function socket(string $socket)
	{
		$this->addArgument('socket', $socket);
		
		return $this;
	}
	
	public function skipSsl()
	{
		$this->addArgument('skip-ssl');
		
		return $this;
	}
	
	/**
	 * Enable SSL for connection (automatically enabled with other flags). Disable with --skip-ssl
	 *
	 * @return $this
	 */
	public function ssl()
	{
		$this->addArgument('ssl');
		
		return $this;
	}
	
	/**
	 * CA file in PEM format
	 *
	 * @param string $sslCa
	 *
	 * @return $this
	 */
	public function sslCa(string $sslCa)
	{
		$this->addArgument('ssl-ca', $sslCa);
		
		return $this;
	}
	
	/**
	 * CA directory
	 *
	 * @param string $sslCaPath
	 *
	 * @return $this
	 */
	public function sslCaPath(string $sslCaPath)
	{
		$this->addArgument('ssl-capath', $sslCaPath);
		
		return $this;
	}
	
	/**
	 * X509 cert in PEM format
	 *
	 * @param string $sslCert
	 *
	 * @return $this
	 */
	public function sslCert(string $sslCert)
	{
		$this->addArgument('ssl-cert', $sslCert);
		
		return $this;
	}
	
	/**
	 * SSL cipher to use
	 *
	 * @param string $sslCipher
	 *
	 * @return $this
	 */
	public function sslCipher(string $sslCipher)
	{
		$this->addArgument('ssl-cipher', $sslCipher);
		
		return $this;
	}
	
	/**
	 * X509 key in PEM format
	 *
	 * @param string $sslKey
	 *
	 * @return $this
	 */
	public function sslKey(string $sslKey)
	{
		$this->addArgument('ssl-key', $sslKey);
		
		return $this;
	}
	
	/**
	 * Certificate revocation list
	 *
	 * @param string $sslCrl
	 *
	 * @return $this
	 */
	public function sslCrl(string $sslCrl)
	{
		$this->addArgument('ssl-crl', $sslCrl);
		
		return $this;
	}
	
	/**
	 * Certificate revocation list path
	 *
	 * @param string $sslCrlPath
	 *
	 * @return $this
	 */
	public function sslCrlPath(string $sslCrlPath)
	{
		$this->addArgument('ssl-crlpath', $sslCrlPath);
		
		return $this;
	}
	
	/**
	 * Verify server’s 'Common Name' in its cert against hostname used when connecting. This option is disabled by default.
	 *
	 * @return $this
	 */
	public function sslVerifyServerCert()
	{
		$this->addArgument('ssl-verify-server-cert');
		
		return $this;
	}
	
	/**
	 * Execute the statement and quit. Disables --force and history file. The default output format is like that produced with --batch.
	 *
	 * @param string $execute
	 *
	 * @return $this
	 */
	public function execute(string $execute)
	{
		$this->addArgument('execute', $execute);
		
		return $this;
	}
}