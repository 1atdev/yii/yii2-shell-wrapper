<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

use IsAtDev\ShellWrapper\Commands\CommandInterface;

class Passthru implements Runner, ReturnValue, ResultCode
{
	protected $_returnValue;
	protected $_resultCode;
	
	public function run(CommandInterface $command)
	{
		$this->_returnValue = passthru((string)$command, $this->_resultCode);
	}
	
	public function getReturnValue()
	{
		return $this->_returnValue;
	}
	
	public function getResultCode(): ?int
	{
		return $this->_resultCode;
	}
}
