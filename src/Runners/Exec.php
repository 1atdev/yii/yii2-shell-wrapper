<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

use IsAtDev\ShellWrapper\Commands\CommandInterface;

class Exec implements Runner, ReturnValue, ResultCode
{
	private $_returnValue;
	private $_resultCode;
	
	public function run(CommandInterface $command)
	{
		exec((string)$command, $this->_returnValue, $this->_resultCode);
	}
	
	public function getReturnValue()
	{
		return $this->_returnValue;
	}
	
	public function getResultCode(): ?int
	{
		return $this->_resultCode;
	}
}
