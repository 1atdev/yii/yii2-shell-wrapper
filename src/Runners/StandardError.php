<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

interface StandardError
{
	public function getStandardError(): mixed;
}
