<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

use IsAtDev\ShellWrapper\Commands\CommandInterface;

class ShellExec implements Runner, ReturnValue
{
	protected $_returnValue;
	
	public function run(CommandInterface $command)
	{
		$this->_returnValue = shell_exec((string)$command);
	}
	
	public function getReturnValue()
	{
		return $this->_returnValue;
	}
}
