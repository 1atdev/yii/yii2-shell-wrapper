<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

interface ResultCode
{
	public function getResultCode(): ?int;
}
