<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

use IsAtDev\ShellWrapper\Commands\CommandInterface;

class System implements Runner, ReturnValue
{
	protected int|null $returnValue;
	
	public function run(CommandInterface $command)
	{
		$this->returnValue = null;
		
		return system((string)$command, $this->returnValue);
	}
	
	public function getReturnValue(): ?int
	{
		return $this->returnValue;
	}
}
