<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

interface ReturnValue
{
	public function getReturnValue();
}
