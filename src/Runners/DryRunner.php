<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

use IsAtDev\ShellWrapper\Commands\CommandInterface;

class DryRunner extends FakeRunner
{
	public function run(CommandInterface $command)
	{
		echo $command . PHP_EOL;
		
		return parent::run($command);
	}
}
