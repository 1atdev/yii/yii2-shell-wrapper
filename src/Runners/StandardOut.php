<?php

declare(strict_types=1);

namespace IsAtDev\ShellWrapper\Runners;

interface StandardOut
{
	public function getStandardOut(): mixed;
}
